import { Services } from '../../../Global/Services';
import { Vector2 } from '../../../Global/Vector2';
import { TicTacToeBoard, TicTacToShape, TicTacToeBoardGameState } from './TicTacToeBoard';

export class TicTacToeGameFlow {
    private readonly m_TicTacToeBoard: TicTacToeBoard;

    constructor(i_Services: Services) {
        this.m_TicTacToeBoard = new TicTacToeBoard(i_Services, {
            matrixSize: 3,
            offsetVector: new Vector2(150, 150),
            gap: 100,
            shapeSize: 20
        });
    }

    public AnimateGameFlow() {
        return this.m_TicTacToeBoard.Begin(TicTacToShape.circle, true);
    }

    public Clear() {
        this.m_TicTacToeBoard.Clear();
    }

    public get Message() {
        const state = this.m_TicTacToeBoard.GameState;
        if (state === TicTacToeBoardGameState.exWon) {
            return "X Won!";
        } else if (state === TicTacToeBoardGameState.circleWon) {
            return "O Won!";
        } else {
            return "TIE";
        }
    }
}
