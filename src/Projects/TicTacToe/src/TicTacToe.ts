import { Canvas } from '../../../Engine/Canvas/Canvas';
import { Services } from '../../../Global/Services';
import { Visual } from '../../../Global/Visual';
import { TicTacToeBase } from './TicTacToeBase';

export class TicTacToe {
    constructor() {
        const services = new Services();
        services.AddServiceWithKey(Visual, new Canvas(services));

        new TicTacToeBase(services).AnimateGame();
    }
}
