import { EngineObject } from '../../../Global/EngineObject';
import { Opacity } from '../../../Global/Opacity';
import { Services } from '../../../Global/Services';
import { Vector2 } from '../../../Global/Vector2';
import { Visual } from '../../../Global/Visual';

interface TicTacToeBoardConfig {
    matrixSize: number,
    offsetVector: Vector2,
    gap: number,
    shapeSize: number,
}

export enum TicTacToShape {
    non,
    circle,
    ex,
}
class BoardCellData {
    public readonly Row: number;
    public readonly Col: number;
    private m_Shape = TicTacToShape.non;
    public get Shape() {
        return this.m_Shape;
    }
    public set Shape(i_Val: TicTacToShape) {
        this.m_Shape = i_Val;

        this.m_Cir.opacity = 0;
        this.m_Ex.opacity = 0;

        if (this.m_Shape === TicTacToShape.circle) {
            this.m_Cir.opacity = 1;
        } else if (this.m_Shape === TicTacToShape.ex) {
            this.m_Ex.opacity = 1;
        }
    }
    private m_Cir: EngineObject;
    private m_Ex: EngineObject;
    private m_Rect: EngineObject;

    constructor(i_Services: Services, i_Row: number, i_Col: number, i_Config: TicTacToeBoardConfig, i_OpFactor: Opacity) {
        const vis = i_Services.GetService(Visual);
        this.Row = i_Row;
        this.Col = i_Col;

        const initObj = (i_Obj: EngineObject) => {
            i_Obj.x = this.Row * i_Config.gap + i_Config.gap / 2;
            i_Obj.y = this.Col * i_Config.gap + i_Config.gap / 2;
            i_Obj.AddPosOffset(i_Config.offsetVector);
            i_Obj.AddOpacityFactor(i_OpFactor);
        };

        this.m_Cir = vis.CreateCircle();
        this.m_Cir.Scale = i_Config.shapeSize;
        this.m_Cir.opacity = 0;
        initObj(this.m_Cir);

        this.m_Ex = vis.CreateEx();
        this.m_Ex.Scale = i_Config.shapeSize;
        this.m_Ex.opacity = 0;
        initObj(this.m_Ex);

        this.m_Rect = vis.CreateRectangle(i_Config.gap, i_Config.gap);
        initObj(this.m_Rect);
    }
}
export enum TicTacToeBoardGameState {
    disabled,
    waitingForInput,
    circleWon,
    exWon,
    tie,
}

export class TicTacToeBoard {
    private m_Board: BoardCellData[][] = [];
    private m_GameState = TicTacToeBoardGameState.disabled;
    private m_OpacityFacotr = new Opacity();
    private m_Config: TicTacToeBoardConfig;

    public get GameState() {
        return this.m_GameState;
    }

    constructor(i_Services: Services, i_Config: TicTacToeBoardConfig) {
        this.m_Config = i_Config;

        if (true) { // init board
            for (let row = 0; row < this.m_Config.matrixSize; row++) {
                this.m_Board.push([]);
                for (let col = 0; col < this.m_Config.matrixSize; col++) {
                    this.m_Board[row].push(new BoardCellData(i_Services, row, col, this.m_Config, this.m_OpacityFacotr))
                }
            }
        }
        if (true) {
            const vis = i_Services.GetService(Visual);
            vis.eOnTouchEnd.On(() => {
                if (this.isGoing) {
                    let rowIdx = (vis.TouchPostion.x - this.m_Config.offsetVector.x) / this.m_Config.gap;
                    let colIdx = (vis.TouchPostion.y - this.m_Config.offsetVector.y) / this.m_Config.gap;
                    rowIdx = Math.floor(rowIdx);
                    colIdx = Math.floor(colIdx);
                    this.MakeAMove(rowIdx, colIdx);
                }
            });
        }
    }
    public MakeAMove(rowIdx: number, colIdx: number) {
        if (this.isGoing && rowIdx >= 0 && colIdx >= 0 && rowIdx < this.m_Config.matrixSize && colIdx < this.m_Config.matrixSize) {
            const curr = this.m_Board[rowIdx][colIdx];
            if (curr.Shape === TicTacToShape.non) {
                curr.Shape = this.m_NextShape;

                this.updateGameState();
                if (!this.isGoing) {
                    if (this.m_CallBack) {
                        this.m_CallBack();
                        this.m_CallBack = undefined;
                    }
                } else {
                    this.m_NextShape = this.getInvertedShape(this.m_NextShape);
                    if (this.m_NextShape === this.m_PcShape) {
                        this.MakeAPcMove();
                    }
                }
            }
        }
    }
    private get isGoing() {
        return this.m_GameState === TicTacToeBoardGameState.waitingForInput;
    }
    public MakeAPcMove() {
        const rndCell = this.rndAviableCell;
        this.MakeAMove(rndCell.Row, rndCell.Col);
    }

    private get rndAviableCell() {
        const arr: BoardCellData[] = [];
        for (let row = 0; row < this.m_Config.matrixSize; row++) {
            for (let col = 0; col < this.m_Config.matrixSize; col++) {
                const curr = this.m_Board[row][col];
                if (curr.Shape === TicTacToShape.non) {
                    arr.push(curr);
                }
            }
        }
        return arr[Math.floor(Math.random() * arr.length)];
    }
    public getInvertedShape(i_Shape: TicTacToShape) {
        return i_Shape === TicTacToShape.circle ? TicTacToShape.ex : TicTacToShape.circle;
    }
    private m_NextShape: TicTacToShape;
    private m_PcShape: TicTacToShape;
    public Begin(i_FirstTurnShape: TicTacToShape, i_IsVsPc: boolean) {
        this.m_NextShape = i_FirstTurnShape;
        if (i_IsVsPc) {
            this.m_PcShape = this.getInvertedShape(this.m_NextShape);
        } else {
            this.m_PcShape = undefined;
        }
        this.m_GameState = TicTacToeBoardGameState.waitingForInput;
        for (let row = 0; row < this.m_Config.matrixSize; row++) {
            for (let col = 0; col < this.m_Config.matrixSize; col++) {
                const curr = this.m_Board[row][col];
                curr.Shape = TicTacToShape.non;
            }
        }
        this.m_OpacityFacotr.opacity = 1;

        return new Promise((resolve) => this.m_CallBack = resolve);
    }
    public Clear() {
        this.m_OpacityFacotr.opacity = 0;
    }
    private m_CallBack: () => void;

    private updateGameState() {
        const winningShape = this.isWonDia1 || this.isWonDia2 || this.isWonRow || this.isWonCol;
        if (winningShape === TicTacToShape.circle) {
            this.m_GameState = TicTacToeBoardGameState.circleWon;
        } else if (winningShape === TicTacToShape.ex) {
            this.m_GameState = TicTacToeBoardGameState.exWon;
        } else if (this.isOutOfMoves) {
            this.m_GameState = TicTacToeBoardGameState.tie;
        } else {
            this.m_GameState = TicTacToeBoardGameState.waitingForInput;
        }
    }

    private get isOutOfMoves() {
        for (let row = 0; row < this.m_Config.matrixSize; row++) {
            for (let col = 0; col < this.m_Config.matrixSize; col++) {
                const curr = this.m_Board[row][col];
                if (curr.Shape === TicTacToShape.non) {
                    return false;
                }
            }
        }
        return true;;
    }
    private get isWonCol(): TicTacToShape {
        for (let row = 0; row < this.m_Config.matrixSize; row++) {
            let first: BoardCellData;
            let isWon = true;
            for (let col = 0; col < this.m_Config.matrixSize; col++) {
                const curr = this.m_Board[row][col];
                if (!first) {
                    first = curr;
                }
                if (curr.Shape === TicTacToShape.non) {
                    isWon = false;
                    break;
                }

                if (curr.Shape !== first.Shape) {
                    isWon = false;
                    break;
                }
            }
            if (isWon) {
                return first.Shape;
            }
        }
        return TicTacToShape.non;
    }
    private get isWonRow(): TicTacToShape {
        for (let row = 0; row < this.m_Config.matrixSize; row++) {
            let first: BoardCellData;
            let isWon = true;
            for (let col = 0; col < this.m_Config.matrixSize; col++) {
                const curr = this.m_Board[col][row];
                if (!first) {
                    first = curr;
                }
                if (curr.Shape === TicTacToShape.non) {
                    isWon = false;
                    break;
                }

                if (curr.Shape !== first.Shape) {
                    isWon = false;
                    break;
                }
            }
            if (isWon) {
                return first.Shape;
            }
        }
        return TicTacToShape.non;
    }
    private get isWonDia1(): TicTacToShape {
        let first: BoardCellData;
        let isWon = true;
        for (let i = 0; i < this.m_Config.matrixSize; i++) {
            const curr = this.m_Board[i][i];
            if (!first) {
                first = curr;
            }
            if (curr.Shape === TicTacToShape.non) {
                isWon = false;
                break;
            }

            if (curr.Shape !== first.Shape) {
                isWon = false;
                break;
            }
        }
        if (isWon) {
            return first.Shape;
        }
        return TicTacToShape.non;
    }

    private get isWonDia2(): TicTacToShape {
        let first: BoardCellData;
        let isWon = true;
        for (let i = 0; i < this.m_Config.matrixSize; i++) {
            const curr = this.m_Board[this.m_Config.matrixSize - i - 1][i];
            if (!first) {
                first = curr;
            }

            if (curr.Shape === TicTacToShape.non) {
                isWon = false;
                break;
            }

            if (curr.Shape !== first.Shape) {
                isWon = false;
                break;
            }
        }
        if (isWon) {
            return first.Shape;
        }
        return TicTacToShape.non;
    }
}
