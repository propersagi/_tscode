import { Services } from '../../../Global/Services';
import { TicTacToeGameFlow } from './TicTacToeGameFlow';
import { TicTacToeScreens } from './TicTacToeScreens';

export class TicTacToeBase {
    private m_TicTacToeScreens: TicTacToeScreens;
    private m_TicTacToeGameFlow: TicTacToeGameFlow;

    constructor(i_Services: Services) {
        this.m_TicTacToeScreens = new TicTacToeScreens(i_Services);
        this.m_TicTacToeGameFlow = new TicTacToeGameFlow(i_Services);
    }

    public AnimateGame() {
        return new Promise((resolve) => {
            this.m_TicTacToeScreens.AnimateIntro()
                .then(() => this.m_TicTacToeGameFlow.AnimateGameFlow())
                .then(() => this.m_TicTacToeScreens.AnimateSummary(this.m_TicTacToeGameFlow.Message))
                .then(() => this.m_TicTacToeGameFlow.Clear())
                .then(() => this.AnimateGame());
        });
    }
}
