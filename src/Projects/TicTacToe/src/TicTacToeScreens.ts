import { Services } from '../../../Global/Services';
import { Visual } from '../../../Global/Visual';
import { EngineObject } from '../../../Global/EngineObject';

export class TicTacToeScreens {
    private m_FontSplash1: EngineObject;
    private m_FontSplash2: EngineObject;
    private m_FontSummary1: EngineObject;
    private m_FontSummary2: EngineObject;
    private m_FontSummary3: EngineObject;

    public AnimateIntro(): Promise<void> {
        return new Promise((resolve) => {
            this.m_FontSplash1.opacity = 1;
            this.m_FontSplash2.opacity = 1;
            this.m_Visual.eOnTouchEnd.OnOff(() => {
                this.m_FontSplash1.opacity = 0;
                this.m_FontSplash2.opacity = 0;
                resolve();
            });
        });
    }
    public AnimateSummary(i_Msg: string): Promise<void> {
        return new Promise((resolve) => {
            this.m_FontSummary2.Text = i_Msg;
            this.m_FontSummary1.opacity = 1;
            this.m_FontSummary2.opacity = 1;
            this.m_FontSummary3.opacity = 1;
            this.m_Visual.eOnTouchEnd.OnOff(() => {
                this.m_FontSummary1.opacity = 0;
                this.m_FontSummary2.opacity = 0;
                this.m_FontSummary3.opacity = 0;
                resolve();
            });
        });
    }
    private m_Visual: Visual;
    constructor(i_Services: Services) {
        this.m_Visual = i_Services.GetService(Visual);
        const xCenter = 300;

        if (true) {
            this.m_FontSplash1 = this.m_Visual.CreateFont();
            this.m_FontSplash1.Text = "TicTacToe DEMO";
            this.m_FontSplash1.x = xCenter;
            this.m_FontSplash1.y = 200;
            this.m_FontSplash1.opacity = 0;
        }
        if (true) {
            this.m_FontSplash2 = this.m_Visual.CreateFont();
            this.m_FontSplash2.Text = "Click anywhere to start";
            this.m_FontSplash2.x = xCenter;
            this.m_FontSplash2.y = 250;
            this.m_FontSplash2.opacity = 0;
        }
        if (true) {
            this.m_FontSummary1 = this.m_Visual.CreateFont();
            this.m_FontSummary1.Text = "The Game has ended!";
            this.m_FontSummary1.x = xCenter;
            this.m_FontSummary1.y = 50;
            this.m_FontSummary1.opacity = 0;
        }
        if (true) {
            this.m_FontSummary2 = this.m_Visual.CreateFont();
            this.m_FontSummary2.x = xCenter;
            this.m_FontSummary2.y = 50 + 40;
            this.m_FontSummary2.opacity = 0;
        }
        if (true) {
            this.m_FontSummary3 = this.m_Visual.CreateFont();
            this.m_FontSummary3.Text = "Click anywhere to continue";
            this.m_FontSummary3.x = xCenter;
            this.m_FontSummary3.y = 50 + 40 * 2;
            this.m_FontSummary3.opacity = 0;
        }

    }
}
