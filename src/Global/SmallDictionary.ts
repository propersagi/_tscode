export class SmallDictionary<Key, Val> {
    private m_Keys: Key[];
    private m_Vals: Val[];

    public Clear() {
        if (this.m_Keys) {
            this.m_Keys.length = 0;
            this.m_Vals.length = 0;
        }
    }
    public GetValue(i_Key: Key): Val {
        if (this.m_Keys) {
            for (let i = 0; i < this.m_Keys.length; i++) {
                if (this.m_Keys[i] === i_Key) {
                    return this.m_Vals[i];
                }
            }
        }
        return undefined;
    }
    public GetValueOrCreate(i_Key: Key, i_Creator: () => Val): Val {
        let val = this.GetValue(i_Key);

        if (!val) {
            val = i_Creator();
            this.SetValue(i_Key, val);
        }

        return val;
    }

    public SetValue(i_Key: Key, i_Val: Val): Val {
        if (!this.m_Keys) {
            this.m_Keys = [];
            this.m_Vals = [];
        }
        for (let i = 0; i < this.m_Keys.length; i++) {
            if (this.m_Keys[i] === i_Key) { // override
                const prevVal = this.m_Vals[i];
                this.m_Vals[i] = i_Val;
                return prevVal;
            }
        }
        this.m_Keys.push(i_Key);
        this.m_Vals.push(i_Val);
        return undefined;
    }
}
