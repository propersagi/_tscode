export class Vector2 {
    public x: number;
    public y: number
    constructor(i_X = 0, i_Y = 0) {
        this.x = i_X;
        this.y = i_Y
    }
}
