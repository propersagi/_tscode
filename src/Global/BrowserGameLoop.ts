import { GenericEvent } from './GenericEvent';

export class BrowserGameLoop {
    public readonly eOnTick = new GenericEvent();
    private m_Dt = 0;
    public get Dt() {
        return this.m_Dt;
    }
    private m_CurrentTime = 0;
    public get Time() {
        return this.m_CurrentTime;
    }
    constructor() {
        // https://coderwall.com/p/iygcpa/gameloop-the-correct-way
        let interval = 1000 / 60;
        let lastTime = (new Date()).getTime();
        let currentTime = 0;
        let delta = 0;

        const gameLoop = () => {
            window.requestAnimationFrame(gameLoop);

            currentTime = (new Date()).getTime();
            delta = (currentTime - lastTime);

            if (delta > interval) {
                this.m_Dt = 1 / delta;
                this.m_CurrentTime += this.m_Dt;

                this.eOnTick.Trigger();
                lastTime = currentTime - (delta % interval);
            }
        };
        gameLoop();
    }
}