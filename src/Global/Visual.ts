import { EngineObject } from './EngineObject';
import { GenericEvent } from './GenericEvent';
import { Vector2 } from './Vector2';

export class Visual {
    public readonly eOnTick1Update: GenericEvent;
    public readonly eOnTick2Draw: GenericEvent;
    public readonly eOnTick3AfterDraw: GenericEvent;
    public readonly eOnTouchStart: GenericEvent;
    public readonly eOnTouchEnd: GenericEvent;
    public readonly TouchPostion: Vector2;
    public Dt: number;

    public CreateCircle(): EngineObject {
        throw new Error("Method not implemented.");
    }
    public CreateEx(): EngineObject {
        throw new Error("Method not implemented.");
    }
    public CreateFont(): EngineObject {
        throw new Error("Method not implemented.");
    }
    public CreateRectangle(i_StartWidth: number, i_StartHeight: number): EngineObject {
        throw new Error("Method not implemented.");
    }
}