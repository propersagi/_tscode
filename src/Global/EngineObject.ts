import { Services } from './Services';
import { Vector2 } from './Vector2';
import { Visual } from './Visual';
import { VisualObject } from './VisualObject';
import { Opacity } from './Opacity';

export class EngineObject {
    private m_VisualObject: VisualObject;
    // private m_Services: Services;
    private m_Visual: Visual;

    public x = 0;
    public y = 0;
    public opacity = 1;
    public angle = 0;
    public scaleW = 1;
    public scaleH = 1;

    public set Text(i_Val: string) {
        this.m_VisualObject.Text = i_Val;
    }
    public get Text() {
        return this.m_VisualObject.Text;
    }

    constructor(i_Services: Services, i_VisualObject: VisualObject) {
        this.m_VisualObject = i_VisualObject;
        // this.m_Services = i_Services;
        this.m_Visual = i_Services.GetService(Visual);

        this.m_Visual.eOnTick2Draw.On(() => this.update());
    }

    public get Scale() {
        return this.scaleW;
    }
    public set Scale(i_Val: number) {
        this.scaleW = i_Val;
        this.scaleH = i_Val;
    }
    private update() {
        if (true) {
            this.m_VisualObject.x = this.x;
            this.m_VisualObject.y = this.y;
            this.m_VisualObject.opacity = this.opacity;
            this.m_VisualObject.angle = this.angle;
            this.m_VisualObject.scaleH = this.scaleH;
            this.m_VisualObject.scaleW = this.scaleW;
        }
        if (true) {
            if (this.m_PosOffsetArr) {
                this.m_PosOffsetArr.forEach((v) => {
                    this.m_VisualObject.x += v.x;
                    this.m_VisualObject.y += v.y;
                })
            }

            if (this.m_OpacityFactor) {
                this.m_OpacityFactor.forEach((v) => {
                    this.m_VisualObject.opacity *= v.opacity;
                })
            }
        }
        this.m_VisualObject.Update();
    }

    private m_PosOffsetArr: Vector2[];
    public AddPosOffset(i_V: Vector2) {
        if (!this.m_PosOffsetArr) {
            this.m_PosOffsetArr = [];
        }
        this.m_PosOffsetArr.push(i_V);
    }

    private m_OpacityFactor: Opacity[];
    public AddOpacityFactor(i_Op: Opacity) {
        if (!this.m_OpacityFactor) {
            this.m_OpacityFactor = [];
        }
        this.m_OpacityFactor.push(i_Op);
    }
}
