export class VisualObject {
    public x: number;
    public y: number;
    public opacity: number;
    public angle: number;
    public scaleW: number;
    public scaleH: number;
    public Text = "";

    public Update() { }
}