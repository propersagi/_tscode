class Helper {
    private m_Handlers: Array<(...i_Arr: any[]) => any>;
    private m_Calls: number;

    public get Calls() {
        if (this.m_Calls === undefined) {
            return 0;
        } else {
            return this.m_Calls;
        }
    }
    public ClearAll() {
        this.m_Handlers = undefined;
    }
    public On(i_Handler: (...i_Arr: any[]) => any): void {
        if (!this.m_Handlers) {
            this.m_Handlers = [];
        }
        this.m_Handlers.push(i_Handler);
    }
    public NowOn(i_Handler: (...i_Arr: any[]) => any): void {
        if (!this.m_Handlers) {
            this.m_Handlers = [];
        }
        this.m_Handlers.push(i_Handler);
        i_Handler();
    }

    public OnOff(i_Handler: (...i_Arr: any[]) => any): void {
        if (!this.m_Handlers) {
            this.m_Handlers = [];
        }

        const func = (...i_Arr: any[]) => {
            this.Off(func);
            return i_Handler(...i_Arr);
        };
        this.m_Handlers.push(func);
    }
    public Off(i_Handler: (...i_Arr: any[]) => any): void {
        if (!this.m_Handlers) {
            return;
        }
        this.m_Handlers = this.m_Handlers.filter((h) => h !== i_Handler);
    }

    public Trigger(...i_Arr: any[]): any {
        if (this.m_Calls === undefined) {
            this.m_Calls = 0;
        }
        this.m_Calls++;
        if (this.m_Handlers) {
            let retVal: any;
            this.m_Handlers.slice(0).forEach((h) => retVal = h(...i_Arr));
            return retVal;
        }
    }

    public get IsTriggred(): boolean {
        return !!this.m_Calls;
    }
}
export class GenericEvent extends Helper {
    public NowOn(i_Handler: () => void): void {
        super.NowOn(i_Handler);
    }
    public On(i_Handler: () => void): void {
        super.On(i_Handler);
    }
    public OnOff(i_Handler: () => void): void {
        super.OnOff(i_Handler);
    }
    public Off(i_Handler: () => void): void {
        super.Off(i_Handler);
    }
    public Trigger(): void {
        return super.Trigger();
    }
}
