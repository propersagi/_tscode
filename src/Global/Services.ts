import { Dictionary } from 'typescript-collections';

export class Services {
    private m_Services = new Dictionary<{ new(...i_Args: any[]): any }, object>();

    public AddService(i_Obj: object): void {
        const oldObj = this.m_Services.setValue(i_Obj.constructor as { new(...i_Args: any[]): any }, i_Obj);
        if (oldObj) {
            throw new Error("Serivice allready exsists");
        }
    }
    public AddServiceWithKey<T>(i_Key: { new(...i_Args: any[]): T }, i_Value: T): void {
        const oldObj = this.m_Services.setValue(i_Key, i_Value as any);
        if (oldObj) {
            throw new Error("Serivice allready exsists");
        }
    }

    public GetService<T>(type: { new(...i_Args: any[]): T }): T {
        return this.m_Services.getValue(type) as any;
    }
}
