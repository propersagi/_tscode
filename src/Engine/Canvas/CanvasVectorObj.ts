import { Services } from '../../Global/Services';
import { VisualObject } from '../../Global/VisualObject';

export abstract class CanvasVectorObj extends VisualObject {
    protected m_Ctx: CanvasRenderingContext2D;
    protected abstract draw(): void;

    constructor(i_Services: Services, i_Ctx: CanvasRenderingContext2D) {
        super();
        this.m_Ctx = i_Ctx;
    }
    public Update() {
        if (this.opacity > 0) {
            this.m_Ctx.globalAlpha = this.opacity;
            this.draw();
            this.m_Ctx.globalAlpha = 1;
        }
    }
}
