import { CanvasVectorObj } from './CanvasVectorObj';

export class CanvasEx extends CanvasVectorObj {
    protected draw() {
        // https://stackoverflow.com/questions/12835531/draw-x-word-use-canvas-html
        this.m_Ctx.beginPath();
        this.m_Ctx.moveTo(this.x - this.scaleW, this.y - this.scaleW);
        this.m_Ctx.lineTo(this.x + this.scaleW, this.y + this.scaleW);

        this.m_Ctx.moveTo(this.x + this.scaleW, this.y - this.scaleW);
        this.m_Ctx.lineTo(this.x - this.scaleW, this.y + this.scaleW);

        this.m_Ctx.stroke();
    }
}
