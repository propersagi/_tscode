import { Services } from '../../Global/Services';
import { CanvasVectorObj } from './CanvasVectorObj';

export class CanvasRectangle extends CanvasVectorObj {

    private readonly m_StartWidth: number;
    private readonly m_StartHeight: number;

    constructor(i_Services: Services, i_Ctx: CanvasRenderingContext2D, i_StartWidth: number, i_StartHeight: number) {
        super(i_Services, i_Ctx);
        this.m_StartWidth = i_StartWidth;
        this.m_StartHeight = i_StartHeight;
    }

    protected draw() {
        const w = this.m_StartWidth * this.scaleW;
        const h = this.m_StartHeight * this.scaleH;

        this.m_Ctx.beginPath();
        this.m_Ctx.rect(this.x - w / 2, this.y - h / 2, w, h);
        this.m_Ctx.stroke();
    }
}
