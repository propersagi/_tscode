import { Services } from '../../Global/Services';
import { CanvasVectorObj } from './CanvasVectorObj';

export class CanvasFont extends CanvasVectorObj {

    constructor(i_Services: Services, i_Ctx: CanvasRenderingContext2D) {
        super(i_Services, i_Ctx);
    }

    protected draw() {
        // TO DO! 
        // http://atomicrobotdesign.com/blog/web-development/html5-canvas-you-dont-always-have-to-clear-the-entire-thing/
        // not redraw text obj each tick
        this.m_Ctx.font = "30px Arial";
        this.m_Ctx.textAlign = "center";
        this.m_Ctx.fillText(this.Text, this.x, this.y);
    }
}
