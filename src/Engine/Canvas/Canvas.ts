import { BrowserGameLoop } from '../../Global/BrowserGameLoop';
import { EngineObject } from '../../Global/EngineObject';
import { GenericEvent } from '../../Global/GenericEvent';
import { Services } from '../../Global/Services';
import { Vector2 } from '../../Global/Vector2';
import { Visual } from '../../Global/Visual';
import { CanvasCircle } from './CanvasCircle';
import { CanvasEx } from './CanvasEx';
import { CanvasFont } from './CanvasFont';
import { CanvasRectangle } from './CanvasRectangle';

export class Canvas implements Visual { // canvas wrapper
    private m_Canvas: HTMLCanvasElement;
    private m_Ctx: CanvasRenderingContext2D;
    private m_BrowserGameLoop = new BrowserGameLoop();
    private m_Services: Services;

    public readonly eOnTick1Update = new GenericEvent();
    public readonly eOnTick2Draw = new GenericEvent();
    public readonly eOnTick3AfterDraw = new GenericEvent();
    public readonly eOnTouchStart = new GenericEvent();
    public readonly eOnTouchEnd = new GenericEvent();

    public readonly TouchPostion = new Vector2();
    public get Dt() {
        return this.m_BrowserGameLoop.Dt;
    }
    constructor(i_Services: Services) {
        this.m_Services = i_Services;

        this.m_Canvas = document.getElementById("canvas") as HTMLCanvasElement;
        this.m_Ctx = this.m_Canvas.getContext("2d");

        if (true) { // touch & mouse
            const updateMousePos = (e: MouseEvent) => {
                const rect = this.m_Canvas.getBoundingClientRect();
                this.TouchPostion.x = e.clientX - rect.left;
                this.TouchPostion.y = e.clientY - rect.top;
            };

            this.m_Canvas.addEventListener('mousemove', updateMousePos, false);

            this.m_Canvas.addEventListener("mousedown", (e) => {
                updateMousePos(e);
                this.eOnTouchStart.Trigger();
            }, false);

            this.m_Canvas.addEventListener("mouseup", (e) => {
                updateMousePos(e);
                this.eOnTouchEnd.Trigger();
            }, false);
        }

        this.m_BrowserGameLoop.eOnTick.On(() => this.update());
    }
    public CreateCircle(): EngineObject {
        return new EngineObject(this.m_Services, new CanvasCircle(this.m_Services, this.m_Ctx));
    }
    public CreateEx(): EngineObject {
        return new EngineObject(this.m_Services, new CanvasEx(this.m_Services, this.m_Ctx));
    }
    public CreateFont(): EngineObject {
        return new EngineObject(this.m_Services, new CanvasFont(this.m_Services, this.m_Ctx));
    }
    public CreateRectangle(i_StartWidth: number, i_StartHeight: number): EngineObject {
        return new EngineObject(this.m_Services, new CanvasRectangle(this.m_Services, this.m_Ctx, i_StartWidth, i_StartHeight));
    }

    private update() {
        this.eOnTick1Update.Trigger();
        this.m_Ctx.clearRect(0, 0, this.m_Canvas.width, this.m_Canvas.height);
        this.eOnTick2Draw.Trigger();
        this.eOnTick3AfterDraw.Trigger();
    }
}