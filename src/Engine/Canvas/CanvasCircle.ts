import { CanvasVectorObj } from './CanvasVectorObj';

export class CanvasCircle extends CanvasVectorObj {
    protected draw() {
        this.m_Ctx.beginPath();
        this.m_Ctx.arc(this.x, this.y, this.scaleW, 0, 2 * Math.PI);
        this.m_Ctx.stroke();
    }
}
