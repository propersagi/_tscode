const {
    FuseBox
} = require("fuse-box");
const watch = require('node-watch');
const fs = require("fs");

const projectObj = JSON.parse(fs.readFileSync('../project.json'))
const proectName = projectObj.name;

const fuse = FuseBox.init({
    homeDir: __dirname,
    output: "../$name.js",
    log: {
        showBundledFiles: false,
    },
});


fuse
    .bundle("app")
    .instructions('> ' + './src/Projects/' + proectName + '/main.ts')
    .completed(proc => {
        console.log('\x1b[42m%s\x1b[0m', '[SUCCESS] Bundle of ' + proectName + ' completed successfully.');
        console.log('\x1b[46m%s\x1b[0m', '[WATCHING] ' + proectName + '.');
    })

fuse.run();

watch('./src', {
    recursive: true
}, (evt, name) => {
    fuse.run();
});